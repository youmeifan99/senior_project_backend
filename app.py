from flask import Flask
from database.db import initialize_db
from flask_restful import Api
from resources.routes import initialize_routes


app = Flask(__name__)
api = Api(app)

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/courseManagement_backend'
    # 'host' : 'mongodb+srv://admin:admin1234@project-db.xjuj8.mongodb.net/courseManagement_backend?retryWrites=true&w=majority'
    #  'host' : 'mongodb+srv://taskapp:kickoff1234@cluster0-y68gv.mongodb.net/courseManagement_backend'
}
initialize_db(app)
initialize_routes(api)

if __name__ == '__main__':
    app.run(port=5000, debug=True)

# app.run()
