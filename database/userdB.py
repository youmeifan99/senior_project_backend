from .db import db


class UserDB(db.Document):
    cmuitaccount = db.StringField(required=True)
    student_id = db.StringField(required=True, unique=True)
    firstname_EN = db.StringField(required=True)
    lastname_EN = db.StringField(required=True)
    organization = db.StringField(required=True)
    role_id = db.StringField(required=True)
