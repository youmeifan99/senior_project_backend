from .db import db


class AttendanceList(db.Document):
    courseId = db.StringField(required=True)
    idList = db.ListField(db.StringField(), required=True)
    time = db.StringField(required=True)
