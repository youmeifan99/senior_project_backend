from .db import db
from database.studentdB import Student


class Course(db.Document):
    courseId = db.StringField(required=True, unique=True)
    name = db.StringField(required=True, unique=True)
    color = db.StringField(required=True, unique=False)
    startTime = db.StringField(required=True, unique=False)
    endTime = db.StringField(required=True, unique=False)
    location = db.StringField(required=True, unique=False)
    day = db.ListField(db.StringField(), required=True)
    # teacherAccount = db.StringField(required=True, unique=False)
    students = db.ListField(db.EmbeddedDocumentField(Student))
