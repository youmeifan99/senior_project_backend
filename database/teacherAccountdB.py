from .db import db


class TeacherAccount(db.Document):
    account = db.StringField(required=True)
    firstname_EN = db.StringField(required=True)
    lastname_EN = db.StringField(required=True)
    role_id = db.StringField(required=True)
