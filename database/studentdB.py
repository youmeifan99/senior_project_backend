from .db import db


class Student(db.EmbeddedDocument):
    stuId = db.StringField(required=True, min_length=6)
    firstName = db.StringField(required=True)
    lastName = db.StringField(required=True)
    attendanceRate = db.StringField(required=True)
