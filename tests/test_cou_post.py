import pytest
import requests
import json
from tests.testing_data import payload_dict

url = 'http://127.0.0.1:5000/courses'  # The root url of the flask app

header = {"content-type": "application/json"}

def test_adding_courses():
    #method of App.py
    request = requests.post(url, data=json.dumps(payload_dict), headers=header)
    actual_add_course_status = request.json()
    
    assert request.status_code == requests.codes.ok
    assert actual_add_course_status == {'Status': 'Added course successfully'}