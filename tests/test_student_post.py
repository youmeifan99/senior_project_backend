import pytest
import requests
import json
from tests.testing_data import expected_post_student

url = 'http://127.0.0.1:5000/courses'  # The root url of the flask app

header = {"content-type": "application/json"}

def test_adding_courses():
    #method of App.py
    request = requests.post(url, data=json.dumps(expected_post_student), headers=header)
    actual_add_course_status = request.json()
    
    assert request.status_code == requests.codes.ok
    assert actual_add_course_status == {'Status': 'Added student successfully'}