import pytest
import requests
import json
from tests.testing_data import expected_updating_course

url = 'http://127.0.0.1:5000/course'  # The root url of the flask app

header = {"content-type": "application/json"}

def test_update_course_by_id():
     #method of App.py
     request = requests.put(url+ '/5f71d58925098023df557214',data=json.dumps(expected_updating_course),headers=header)
     actual_update_course_status = request.json()
     assert request.status_code == requests.codes.ok
     assert actual_update_course_status == {'Status': 'Updated course successfully'}

