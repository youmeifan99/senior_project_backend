expected_course = [
    {
        "_id": {
            "$oid": "5f71d58925098023df557214"
        },
        "courseId": "9532088",
        "name": "ASD",
        "color": "#595BD9",
        "startTime": "09:00",
        "endTime": "12:00",
        "location": "RB1011",
        "day": [
            "Monday"
        ],
        "students": [
            {
                "stuId": "602115522",
                "firstName": "Youmai",
                "lastName": "Fan",
                "attendanceRate": ""
            }
        ]
    }
]

payload_dict = {
   "isPostingCourse": "Course",
   "name":"ADT",
   "color":"#595BD9",
   "startTime":"12:00",
   "endTime":"15:00",
   "location":"CAMT112",
   "courseId":"9532333",
   "day":[
      "Monday",
      "Friday"
   ],
   "students":[
      {
         "stuId":"602115512",
         "firstName":"Siyang",
         "lastName":"Huang",
         "attendanceRate":""
      }
   ]
}
expected_course_id = "953011"

expected_updating_course = {
   "name":"ASD 2",
   "color":"#595BD9",
   "startTime":"09:00",
   "endTime":"12:00",
   "location":"RB1011",
   "courseId":"9532088",
   "day":[
      "Monday"
   ],
   "students":[
      {
         "stuId":"602115522",
         "firstName":"Youmai",
         "lastName":"Fan",
         "attendanceRate":""
      }
   ]
}


expected_delete_course = {
    "isDeletingCourse": "Course",
    "courseId": "9532333"
}

expected_post_student={
   "isPostingCourse": "Student",
   "courseId":"9532088",
   "stuId":"602115523",
   "firstName":"fffffff",
   "lastName":"Ffsdvsdvsan",
   "attendanceRate":""
}

expected_delete_student={
    "isDeletingCourse": "Student",
    "courseId":"9532088",
    "stuId":"602115523",
    "firstName":"fffffff",
    "lastName":"Ffsdvsdvsan",
    "attendanceRate":""
}

post_image={
  "image_1":"",
  "image_2":"",
  "image_3":"",
  "image_4":""
}

expected_name=['Freya']