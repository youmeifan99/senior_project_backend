import pytest
import requests
import json
from resources.faceRecDao import match_name_list
from tests.testing_data import post_image,expected_name
import known_faces
import base64

with open("/Users/huangsiyang/senior_project_backend/known_faces/Freya/freya2.JPG", "rb") as img_file:
    my_string = base64.b64encode(img_file.read())
    p_string=my_string.decode('utf-8')
    z_string='"' +p_string+'"'


url='http://127.0.0.1:5000/images'
header = {"content-type": "application/json"}


def test_face_reco():
    

    request = requests.post(url, data=json.dumps( {"image_1":p_string,
  "image_2":"",
  "image_3":"",
  "image_4":""}), headers=header)
    actual_post_image_status = request.json()
    
    
    
    assert match_name_list == expected_name