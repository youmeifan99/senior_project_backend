import pytest
import requests
import json
from tests.testing_data import expected_delete_course

url = 'http://127.0.0.1:5000/courses'  # The root url of the flask app

header = {"content-type": "application/json"}

def test_delete_course_by_id():
     #method of App.py
     request = requests.delete(url,data=json.dumps(expected_delete_course), headers=header)
     actual_delete_course_status = request.json()
     assert request.status_code == requests.codes.ok
     assert actual_delete_course_status == {'Status': 'Deleted course successfully'}
