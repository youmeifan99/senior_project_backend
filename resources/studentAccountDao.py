from flask import Response, request
from database.studentAccountdB import StudentAccount
from flask.json import jsonify
from flask_restful import Resource


class findAllStudents(Resource):
    def get(self):
        studentAccount = StudentAccount.objects().order_by('student_id')
        return Response(studentAccount.to_json(), mimetype="application/json", status=200)


class addStudentsInfo(Resource):
    def post(self):
        body = request.get_json()
        studentAccount = StudentAccount(**body).save()
        id = studentAccount.student_id
        return {'id': str(id)}, 200


class searchById(Resource):
    def get(self, id):
        student = StudentAccount.objects.get(id=id).to_json()
        return Response(student, mimetype="application/json", status=200)
