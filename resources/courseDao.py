from flask import Response, request
from database.coursedB import Course
from database.studentdB import Student
from flask.json import jsonify
from flask_restful import Resource
import datetime


class findAllCourse(Resource):
    def get(self):
        course = Course.objects().order_by('courseId')
        return Response(course.to_json(), mimetype="application/json", status=200)


class addCourseInfo(Resource):
    def post(self):
        body = request.get_json(force=True)
        isPostingCourse = body.get('isPostingCourse')
        if isPostingCourse == 'Course':
            courseId = body.get('courseId')
            name = body.get('name')
            color = body.get('color')
            startTime = body.get('startTime')
            endTime = body.get('endTime')
            location = body.get('location')
            day = body.get('day')
            students = body.get('students')
            teacherAccount = body.get('teacherAccount')
            course = Course(name=name, color=color, startTime=startTime, endTime=endTime,
                            location=location, courseId=courseId, day=day, students=students)
            course.save()
            return Response(course.to_json(), mimetype="application/json", status=200)
        elif isPostingCourse == 'Student':
            courseId = body.get('courseId')
            stuId = body.get('stuId')
            firstName = body.get('firstName')
            lastName = body.get('lastName')
            attendanceRate = body.get('attendanceRate')
            student = Student(
                stuId=stuId, firstName=firstName, lastName=lastName, attendanceRate=attendanceRate)
            studentCourse = Course.objects(courseId=courseId)
            studentCourse.update(push__students=student)
            return Response(student.to_json(), mimetype="application/json", status=200)


class updateCourseById(Resource):
    def put(self, id):
        body = request.get_json()
        Course.objects.get(id=id).update(**body)
        return {'Status': 'Updated course successfully'}


class deleteCourseById(Resource):
    def delete(self):
        body = request.get_json(force=True)
        isDeletingCourse = body.get('isDeletingCourse')
        if isDeletingCourse == 'Course':
            courseId = body.get('courseId')
            course = Course.objects(courseId=courseId)
            course.delete()
            return {'Status': 'Deleted course successfully'}
        elif isDeletingCourse == 'Student':
            courseId = body.get('courseId')
            stuId = body.get('stuId')
            firstName = body.get('firstName')
            lastName = body.get('lastName')
            attendanceRate = body.get('attendanceRate')
            student = Student(
                stuId=stuId, firstName=firstName, lastName=lastName, attendanceRate=attendanceRate)
            studentCourse = Course.objects(
                courseId=courseId, students=student)
            studentCourse.update(pull__students=student)
            return {'Status': 'Deleted student successfully'}


class findCourseById(Resource):
    def get(self, id):
        courses = Course.objects.get(id=id).to_json()
        return Response(courses, mimetype="application/json", status=200)
