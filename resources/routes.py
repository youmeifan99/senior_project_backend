from .courseDao import *
from .imageProcessorDao import *
from .teahcerDao import *
from .studentAccountDao import addStudentsInfo, findAllStudents, searchById
from .attendanceListDao import *
from .UserDao import UserDao
# from .student import StudentApi


def initialize_routes(api):
    api.add_resource(findAllCourse, '/courses')
    api.add_resource(addCourseInfo, '/courses')
    api.add_resource(updateCourseById, '/course/<id>')
    api.add_resource(deleteCourseById, '/courses')
    api.add_resource(findCourseById, '/course/<id>')
    api.add_resource(getImageData, '/images')
    api.add_resource(findAllTeacher, '/teacher')
    api.add_resource(addTeacherInfo, '/teacher')
    api.add_resource(addStudentsInfo, '/student')
    api.add_resource(findAllStudents, '/student')
    api.add_resource(searchById, '/student/<id>')
    api.add_resource(updateCheckAttendance, '/student/check-attendance')
    api.add_resource(getCheckAttendance, '/student/check-attendance')
    api.add_resource(UserDao, '/authentication')

    # api.add_resource(StudentApi, '/courses/students')
    # api.add_resource(StudentApi, '/course/<id>/student/<ids>')
