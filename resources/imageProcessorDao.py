from flask import Response, request
from flask.json import jsonify
from flask_restful import Resource, reqparse
import os
import base64
from io import BytesIO
from PIL import Image
from resources.faceRecognitionDao import *

images_base64 = []
outpath = "unknown_faces/"
images = []
match = None
match_name_list = []


class getImageData(Resource):
    def __init__(self):
        # Create a request parser
        parser = reqparse.RequestParser()
        parser.add_argument("image_1", type=str,
                            help="Base64 encoded image string", required=True, location='json')
        parser.add_argument("image_2", type=str,
                            help="Base64 encoded image string", required=True, location='json')
        parser.add_argument("image_3", type=str,
                            help="Base64 encoded image string", required=True, location='json')
        parser.add_argument("image_4", type=str,
                            help="Base64 encoded image string", required=True, location='json')
        self.req_parser = parser

    def post(self):
        image_1 = self.req_parser.parse_args(strict=True).get("image_1", None)
        image_2 = self.req_parser.parse_args(strict=True).get("image_2", None)
        image_3 = self.req_parser.parse_args(strict=True).get("image_3", None)
        image_4 = self.req_parser.parse_args(strict=True).get("image_4", None)

        images_base64.append(image_1)
        images_base64.append(image_2)
        images_base64.append(image_3)
        images_base64.append(image_4)

        match_name_list.clear()

        i = 1
        for image in images_base64:
            if image is not None:
                starter = image.find(',')
                image_data = image[starter+1:]
                image_data = bytes(image_data, encoding="ascii")
                im = Image.open(BytesIO(base64.b64decode(image_data)))
                save_fname = os.path.join(
                    outpath, os.path.basename('image_' + str(i))+'.JPG')
                im.save(save_fname)
                i += 1
                print('Save images successful!!!')

                match_name = faceRec()
                for match in match_name:

                    if match in match_name_list:
                        print('Name already exists!!!')
                    else:
                        match_name_list.append(match)

                # if match_name in match_name_list:
                #     print('Name already exists!!!')
                # else:
                #     match_name_list.append(match_name)

                remove_img(save_fname)

            else:
                print("Empty image!!")

        images_base64.clear()
        return match_name_list
        # return Response(match_name_list.to_json(), mimetype="application/json", status=200)
