from flask import Response, request
from database.coursedB import Course
from database.attendanceListdB import AttendanceList
from flask.json import jsonify
from flask_restful import Resource
from mongoengine.errors import DoesNotExist


class updateCheckAttendance(Resource):
    def post(self):
        body = request.get_json(force=True)
        courseId = body.get('courseId')
        idList = body.get('idList')
        time = body.get('time')
        try:
            attendance_model = AttendanceList.objects(
                courseId=courseId, time=time).update(add_to_set__idList=idList)
            attendance_updated = AttendanceList.objects(
                courseId=courseId, time=time).get()
            return Response(attendance_updated.to_json(), mimetype="application/json", status=200)
        except DoesNotExist:
            attendance_model = AttendanceList(**body)
            attendance_model.save()
            return Response(attendance_model.to_json(), mimetype="application/json", status=200)


class getCheckAttendance(Resource):
    def get(self):
        attendanceList = AttendanceList.objects().order_by('courseId')
        return Response(attendanceList.to_json(), mimetype="application/json", status=200)
