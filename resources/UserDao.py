from flask import Response, request
from database.userdB import UserDB
from flask.json import jsonify
from flask_restful import Resource
from mongoengine.queryset import DoesNotExist


class UserDao(Resource):

    def post(self):
        body = request.get_json(force=True)
        cmuitaccount = body.get('cmuitaccount')
        student_id = body.get('student_id')
        firstname_EN = body.get('firstname_EN')
        lastname_EN = body.get('lastname_EN')
        role_id = body.get('role_id')
        organization = body.get('organization')
        try:
            user_model = UserDB.objects(cmuitaccount=cmuitaccount).get()
            user_model.update(**body)
            user_model.reload()
            return Response(user_model.to_json(), mimetype="application/json", status=200)
        except DoesNotExist:
            user_model = UserDB(**body)
            user_model.save()
            return Response(user_model.to_json(), mimetype="application/json", status=200)
