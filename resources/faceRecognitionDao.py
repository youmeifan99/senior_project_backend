import face_recognition
import os
import cv2
import requests


KNOWN_FACES_DIR = 'known_faces'
UNKNOWN_FACES_DIR = 'unknown_faces'
TOLERANCE = 0.45
FRANE_THICHNESS = 3
FONT_THINCKNESS = 2
MODEL = 'hog'


def faceRec():
    print('Loading known faces...')
    known_faces = []
    known_names = []
    match = None
    match_name = []

    for name in os.listdir(KNOWN_FACES_DIR):

        # Next we load every file of faces of known person
        for filename in os.listdir(f'{KNOWN_FACES_DIR}/{name}'):

            # Load an image
            image = face_recognition.load_image_file(
                f'{KNOWN_FACES_DIR}/{name}/{filename}')

            # Get 128-dimension face encoding
            # Always returns a list of found faces, for this purpose we take first face only (assuming one face per image as you can't be twice on one image)
            encoding = face_recognition.face_encodings(image)[0]

            # Append encodings and name
            known_faces.append(encoding)
            known_names.append(name)

    print('Processing unknown faces...')
    # Now let's loop over a folder of faces we want to label
    for filename in os.listdir(UNKNOWN_FACES_DIR):

        # Load image
        print(f'Filename {filename}', end='')
        image = face_recognition.load_image_file(
            f'{UNKNOWN_FACES_DIR}/{filename}')

        # This time we first grab face locations - we'll need them to draw boxes
        locations = face_recognition.face_locations(image, model=MODEL)

        # Now since we know loctions, we can pass them to face_encodings as second argument
        # Without that it will search for faces once again slowing down whole process
        encodings = face_recognition.face_encodings(image, locations)

        # We passed our image through face_locations and face_encodings, so we can modify it
        # First we need to convert it from RGB to BGR as we are going to work with cv2
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # But this time we assume that there might be more faces in an image - we can find faces of dirrerent people
        # print('\n')
        print(f', found {len(encodings)} face(s)')
        for face_encoding, face_location in zip(encodings, locations):

            # We use compare_faces (but might use face_distance as well)
            # Returns array of True/False values in order of passed known_faces
            results = face_recognition.compare_faces(
                known_faces, face_encoding, TOLERANCE)
            print(results)

            if True in results:
                match = known_names[results.index(True)]
                print(f"Match found:{match}")

                if match in match_name:
                    print('Name already exists!!!')
                else:
                    match_name.append(match)

                # top_left = (face_location[3], face_location[0])
                # bottom_right = (face_location[1], face_location[0])
                # color = [0, 255, 0]
                # cv2.rectangle(image, top_left, bottom_right,
                #               color, FRANE_THICHNESS)

                # top_left = (face_location[3], face_location[2])
                # bottom_right = (face_location[1], face_location[2]+22)
                # cv2.rectangle(image, top_left, bottom_right,
                #               color, cv2.FILLED)
                # cv2.putText(
                #     image, match, (face_location[3]+10, face_location[2]+15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 200), FONT_THINCKNESS)

                # cv2.imshow(filename, image)
                # cv2.waitKey(10000)
                # cv2.destroyWindow(filename)

    print('match name:', match)
    return match_name


def remove_img(file):
    os.remove(file)
    print('Delete images successful!!')
