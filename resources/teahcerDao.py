from flask import Response, request
from database.teacherAccountdB import TeacherAccount
from flask.json import jsonify
from flask_restful import Resource


class findAllTeacher(Resource):
    def get(self):
        teacher = TeacherAccount.objects().order_by('teacherId')
        return Response(teacher.to_json(), mimetype="application/json", status=200)


class addTeacherInfo(Resource):
    def post(self):
        body = request.get_json()
        teacher = TeacherAccount(**body).save()
        id = teacher.id
        return {'id': str(id)}, 200
